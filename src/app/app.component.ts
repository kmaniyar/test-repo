import { Component } from '@angular/core';

@Component(
  {
    selector: 'dhruv-jariwala',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'  ]
  }
)

export class AppComponent {

  firstName : string = 'Khilen';
  lastName : string = 'Maniyar';

  arr: number[] = [5,10,15,20,25];

  obj = {
    firstName: 'Urvisha',
    lastName: 'Pandav',
    age: 34,
    hobbies: [
      'Cricket',
      'Chess'
    ],
    skills: {
      desc: 'This Fields provides detail of the Skills of a Student',
      updatedBy: 'Last, this desc was updated by Rahul',
      arr : [
        {
          id: 1,
          name: 'Angular',
          yearOfExp: '5'
        }
      ]
    }

    //objTemp.META.TABLE_NAME

  //   objTemp = { 
  //     DB: 'Test',
  //     META: [ 
  //        { 
  //           'COUNT':'1',
  //           'TABLE_NAME':'V_USER_LOGIN',
  //           'AUTH_TOKEN':'187f6d62-70b5-421e-bc43-d85dc4c3d2fd1570850032998',
  //           'PAGE_NO':'1',
  //           'RECORD_END_NO':'50',
  //           'RECORD_START_NO':'1'
  //        },
  //        { 
  //         'COUNT':'2',
  //         'TABLE_NAME':'V_USER_LOGIN',
  //         'AUTH_TOKEN':'187f6d62-70b5-421e-bc43-d85dc4c3d2fd1570850032998',
  //         'PAGE_NO':'1',
  //         'RECORD_END_NO':'50',
  //         'RECORD_START_NO':'1'
  //      }
  //     ],
  //     DATA:[ 
  //        { 
  //           'ID':'1',
  //           'USER_NAME':'jss',
  //           'PASSWORD':'!K!!M$%%O(*++Q/2455S:>ACDDUJOSVXYYW`fkortuuY',
  //           'USER_TYPE':'H',
  //           'ROLE_MSTR_ID':'100121',
  //           'ROLE_NAME':'',
  //           'ZONE_ID':'100060',
  //           'ZONE_NAME':'AHMEDABAD',
  //           'SUB_ZONE_ID':'100000',
  //           'SUB_ZONE_NAME':'Rajkot',
  //           'REGION_ID':'100000',
  //           'REGION_NAME':'Salvivad',
  //           'SUB_REGION_ID':'100000',
  //           'SUB_REGION_NAME':'par',
  //           'MANDAL_ID':'1',
  //           'MANDAL_NAME':'Abjibapanagar',
  //           'ZONING_LEVEL':'M',
  //           'KARYALAY_ID':'',
  //           'AREA_ID':'100093',
  //           'AREA_NAME':'VASNA',
  //           'CITY_ID':'100003',
  //           'CITY_NAME':'AHMEDABAD',
  //           'DISTRICT_ID':'100003',
  //           'DISTRICT_NAME':'AHMEDABAD',
  //           'STATE_ID':'100005',
  //           'STATE_NAME':'GUJARAT',
  //           'COUNTRY_ID':'100004',
  //           'COUNTRY_NAME':'INDIA',
  //           'RNUM':'1'
  //        }
  //     ]
  //  }


  }


  constructor() {

  }
}